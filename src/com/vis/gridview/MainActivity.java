package com.vis.gridview;

import com.vis.gridview.adapter.CustomGridView;

import android.support.v7.app.ActionBarActivity;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends Activity {
    GridView grid;
    String[] web = {
            "Cost",
            "Gold",
            "Court",
            "Music",
            "Skype",
            "FaceBook",
            "IMO",
            "Call",
            "Whats APP",
            "WordPress",
            "Google Plus",
            "Iphone",
            "bank",
            "Saving",
            "Money",
            "Cost",
            "Gold",
            "Court",
            "Music",
            "Skype",
            "FaceBook",
            "IMO",
            "Call",
            "Whats APP",
            "WordPress",
            "Google Plus",
            "Iphone",
            "bank",
            "Saving",
            "Money"
 
    } ;
    int[] imageId = {
            R.drawable.a1,
            R.drawable.a2,
            R.drawable.a3,
            R.drawable.a4,
            R.drawable.a5,
            R.drawable.a6,
            R.drawable.a7,
            R.drawable.a8,
            R.drawable.a9,
            R.drawable.a10,
            R.drawable.a11,
            R.drawable.a12,
            R.drawable.bank,
            R.drawable.banking,
            R.drawable.a2,
            R.drawable.a1,
            R.drawable.a2,
            R.drawable.a3,
            R.drawable.a4,
            R.drawable.a5,
            R.drawable.a6,
            R.drawable.a7,
            R.drawable.a8,
            R.drawable.a9,
            R.drawable.a10,
            R.drawable.a11,
            R.drawable.a12,
            R.drawable.bank,
            R.drawable.banking,
            R.drawable.a2
 
    };
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
 
        CustomGridView adapter = new CustomGridView(MainActivity.this, web, imageId);
        grid=(GridView)findViewById(R.id.grid);
                grid.setAdapter(adapter);
                grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
 
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        Toast.makeText(MainActivity.this, "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();
 
                    }

					
                });
 
    }
 
}